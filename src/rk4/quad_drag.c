/* Computes dv/dt of a projectile with quadratic air resistance
 *
 * Aaron Bunch, Nov 2019
 *
 * dv/dt = -(0.5 * rho * Cd * A / mass) * |v| * v - g
 *
 * rho          atmospheric density
 * Cd           drag coefficient
 * A            effective area
 * mass         mass of the projectile
 * |v|          magnitude of the velocity vector
 * v            velocity vector
 * g            acceleration due to gravity vector
 * constants    -(0.5 * rho * Cd * A / mass)   
 */

#include <math.h>
#include "quad_drag.h"

static double g[3] = { 0, 0, 9.81 };

void derivs(double *v, double *dvdt, double constants)
{
    double mag_v = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
    dvdt[0] = constants * mag_v * v[0] - g[0];
    dvdt[1] = constants * mag_v * v[1] - g[1];
    dvdt[2] = constants * mag_v * v[2] - g[2];
}

