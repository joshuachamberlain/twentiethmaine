// Implements 4th-order Runge-Kutta integrator for a second-order ODE
//
// Aaron Bunch, Nov 2019

#include "rk4.h"
#include "quad_drag.h"

#define NDIM 3

static double k0[NDIM] = { 0 };
static double l0[NDIM] = { 0 };
static double k1[NDIM] = { 0 };
static double l1[NDIM] = { 0 };
static double k2[NDIM] = { 0 };
static double l2[NDIM] = { 0 };
static double k3[NDIM] = { 0 };
static double l3[NDIM] = { 0 };
static double tmp[NDIM] = { 0 };

void rk4(double *r, double *v, double *dvdt, double h, double *r_out,
         double *v_out, void (*derivs)(double *, double *, double),
         double constants)
{
    // dr/dt = v
    // dv/dt = C * |v| * v - g

    // k0 = h * v 
    k0[0] = h * v[0];
    k0[1] = h * v[1];
    k0[2] = h * v[2];

    // l0 = h * dv/dt
    (*derivs)(v, dvdt, constants);
    l0[0] = h * dvdt[0];
    l0[1] = h * dvdt[1];
    l0[2] = h * dvdt[2];

    // k1 = h * (v + 0.5 * l0)
    k1[0] = h * (v[0] + 0.5 * l0[0]);
    k1[1] = h * (v[1] + 0.5 * l0[1]);
    k1[2] = h * (v[2] + 0.5 * l0[2]);

    // l1 = h * dv/dt(v + 0.5 * l0) 
    tmp[0] = v[0] + 0.5 * l0[0];
    tmp[1] = v[1] + 0.5 * l0[1];
    tmp[2] = v[2] + 0.5 * l0[2];
    (*derivs)(tmp, dvdt, constants);
    l1[0] = h * dvdt[0];
    l1[1] = h * dvdt[1];
    l1[2] = h * dvdt[2];

    // k2 = h * (v + 0.5 * l1)
    k2[0] = h * (v[0] + 0.5 * l1[0]);
    k2[1] = h * (v[1] + 0.5 * l1[1]);
    k2[2] = h * (v[2] + 0.5 * l1[2]);

    // l2 = h * dv/dt(v + 0.5 * l1) 
    tmp[0] = v[0] + 0.5 * l1[0];
    tmp[1] = v[1] + 0.5 * l1[1];
    tmp[2] = v[2] + 0.5 * l1[2];
    (*derivs)(tmp, dvdt, constants);
    l2[0] = h * dvdt[0];
    l2[1] = h * dvdt[1];
    l2[2] = h * dvdt[2];

    // k3 = h * (v + l2)
    k3[0] = h * (v[0] + l2[0]);
    k3[1] = h * (v[1] + l2[1]);
    k3[2] = h * (v[2] + l2[2]);

    // l3 = h * dv/dt(v + l2) 
    tmp[0] = v[0] + l2[0];
    tmp[1] = v[1] + l2[1];
    tmp[2] = v[2] + l2[2];
    (*derivs)(tmp, dvdt, constants);
    l3[0] = h * dvdt[0];
    l3[1] = h * dvdt[1];
    l3[2] = h * dvdt[2];

    // r_out = r + 1/6 * (k0 + 2k1 + 2k2 + k3)
    r_out[0] = r[0] + (1.0 / 6.0) * (k0[0] + 2*(k1[0] + k2[0]) + k3[0]);
    r_out[1] = r[1] + (1.0 / 6.0) * (k0[1] + 2*(k1[1] + k2[1]) + k3[1]);
    r_out[2] = r[2] + (1.0 / 6.0) * (k0[2] + 2*(k1[2] + k2[2]) + k3[2]);

    // v_out = v + 1/6 * (l0 + 2l1 + 2l2 + l3)
    v_out[0] = v[0] + (1.0 / 6.0) * (l0[0] + 2*(l1[0] + l2[0]) + l3[0]);
    v_out[1] = v[1] + (1.0 / 6.0) * (l0[1] + 2*(l1[1] + l2[1]) + l3[1]);
    v_out[2] = v[2] + (1.0 / 6.0) * (l0[2] + 2*(l1[2] + l2[2]) + l3[2]);
}
