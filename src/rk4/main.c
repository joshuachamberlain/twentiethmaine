/* Driver for rk4.c
 * 
 * Aaron bunch, Dec 2019
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "rk4.h"
#include "quad_drag.h"

#define PI 3.1415927
#define SIM_TIME 20     // sec
#define TIME_STEP 0.1   // sec
#define X0 0 
#define Y0 0 
#define Z0 0 
#define MAG_V 300       // m/s    
#define THETA 35        // deg
#define VX0 MAG_V * cos(THETA * PI/180) 
#define VY0 0 
#define VZ0 MAG_V * sin(THETA * PI/180) 
#define A 0.01          // m^2 
#define CD 0.5          // sphere   
#define RHO 1.25        // sea level 
#define MASS 12 / 2.2   // kg (12 pounds) 
#define NDIM 3
#define OUT_PATH "/home/aaron/twentiethmaine/data/rk4data"

int main(int argc, char *argv[])
{
   // setup
   double total_simtime = SIM_TIME;
   double h = TIME_STEP;
   char fname[] = OUT_PATH;
   FILE *fp = fopen(fname, "w");
   if (fp == NULL) {
       perror(fname);
       exit(EXIT_FAILURE);
   }
   
   // set initial conditions
   double r[] = { X0, Y0, Z0 };
   double v[] = { VX0, VY0, VZ0 };
   double dvdt[NDIM] = { 0 };
   double r_out[NDIM] = { 0 };
   double v_out[NDIM] = { 0 };
   double constants;
   double Cd = CD;
   double rho = RHO;
   double mass = MASS;
   
   // print column headers
   fprintf(fp, "%15s%15s%15s%15s%15s%15s%15s\n",
               "time", "x", "y", "z", "vx", "vy", "vz");

   // print initial positions
   fprintf(fp, "%15.3f", 0.0);
   fprintf(fp, "%15.6f%15.6f%15.6f", r[0], r[1], r[2]);
   fprintf(fp, "%15.6f%15.6f%15.6f\n", v[0], v[1], v[2]);

   // loop through the time-steps
   double t;
   for (t = 0.0; t <= total_simtime; t +=h) {
       // compute -(0.5*Cd*rho*A/mass)
       constants = -(0.5 * Cd * rho * A / mass);
       // integrate
       rk4(r, v, dvdt, h, r_out, v_out, derivs, constants);
       // update r, v
       r[0] = r_out[0];
       r[1] = r_out[1];
       r[2] = r_out[2];
       v[0] = v_out[0];
       v[1] = v_out[1];
       v[2] = v_out[2];
       // print results
       fprintf(fp, "%15.3f", t+h);
       fprintf(fp, "%15.6f%15.6f%15.6f", r_out[0], r_out[1], r_out[2]);
       fprintf(fp, "%15.6f%15.6f%15.6f\n", v_out[0], v_out[1], v_out[2]);
   }
   fclose(fp);
   return EXIT_SUCCESS;
}
