/* Header file for rk4.c
 *
 * Inputs: r            pointer to position vector
 *         v            pointer to velocity vector
 *         dvdt         pointer to dv/dt vector 
 *         h            time step
 *         derivs       pointer to a function that computes dv/dt 
 *         constants    to pass to derivs() 
 *
 * Outputs:
 *         r_out        pointer to updated position vector
 *         v_out        pointer to updated velocity vector
 */         

#pragma once

#include "quad_drag.h"

void rk4(double *r, double *v, double *dvdt, double h, double *r_out,
         double *v_out, void (*derivs)(double *, double *, double),
         double constants);

